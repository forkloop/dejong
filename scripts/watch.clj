(require '[cljs.build.api :as b])

(b/watch "src"
  {:main 'dejong.core
   :output-to "out/dejong.js"
   :output-dir "out"})
