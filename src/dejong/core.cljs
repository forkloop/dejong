(ns dejong.core
  (:require [clojure.browser.repl :as repl]))

;; (defonce conn
;;   (repl/connect "http://localhost:9000/repl"))

(enable-console-print!)

(println "Hello world!")

(def sensitivity 0.02)
(def *iterations* 8000)
(def *density* 2)
(def *start* 0)
(def *limit* 200)
(def *size* 700)

(def N (* *size* (.-devicePixelRatio js/window)))

(def canvas (.createElement js/document "canvas"))
(set! (.-width canvas) N)
(set! (.-height canvas) N)
(set! (.-width (.-style canvas)) (str *size* "px"))
(set! (.-height (.-style canvas)) (str *size* "px"))
(.appendChild (.-body js/document) canvas)
(def *context* (.getContext canvas "2d"))

(def *mouse-x* *start*)
(def *mouse-y* *start*)

;;
(deftype Sketch [^:mutable steps
                 ^:mutable stopped
                 attractor
                 ^{:mutable true} interval]
  Object
  (stop-loop [_]
    (set! interval (js/clearInterval interval)))

  (tick [_]
    (if stopped
      (.reseed attractor)
      (do
        ;(println steps)
        (set! steps (inc steps))
        (.plot attractor 5)
        (if (> steps *limit*)
          (.stop-loop _)))))

  (loopp [_]
    (set! interval (js/setInterval (.bind (.-tick _) _) 0)))

  (initial-seed [_])

  (pause [this e]
    (.preventDefault e)
    (set! stopped true)
    (if-not interval
      (.loopp this)))

  (record [_ e]
    (if stopped
      (set! *mouse-x* (- (.-pageX e) (.-offsetLeft canvas)))
      (set! *mouse-y* (- (.-pageY e) (.-offsetTop canvas)))))

  (resume [_]
    (set! stopped false)
    (set! steps 0))

  (permalink [_ e]
    (set! (.-hash (.-location js/window)) (str *mouse-x* "," *mouse-y*)))
  )

(defn zero-array [m n]
  (let [arr (array m)]
    (dotimes [i m]
      (let [row (array n)]
        (dotimes [j n]
          (aset row j 0))
        (aset arr i row)))
    arr))

(deftype DeJongAttractor [^:mutable image
                          ^:mutable density
                          ^:mutable max-density
                          ^:mutable x-seed
                          ^:mutable y-seed
                          ^:mutable x
                          ^:mutable y]
  Object
  (clear [_]
    (set! image (.createImageData *context* N N))
    (set! density (zero-array N N))
    (set! max-density 0))

  (seed [_]
    (set! x-seed (* sensitivity (- (/ (* *mouse-x* 2) N) 1)))
    (set! y-seed (* sensitivity (- (/ (* *mouse-y* 2) N) 1)))
    (set! x (/ N 2))
    (set! y (/ N 2)))

  (populate [_ samples]
    (dotimes [i (* samples *iterations*)]
      (let [xx (+ (* (- (.sin js/Math (* x-seed y)) (.cos js/Math (* y-seed x))) N 0.2) (/ N 2))
            yy (+ (* (- (.sin js/Math (* (- x-seed) x)) (.cos js/Math (* (- y-seed) y))) N 0.2) (/ N 2))]
        (let [idx-x (.round js/Math xx)
              idx-y (.round js/Math yy)]
          (aset density idx-x idx-y (+ (aget density idx-x idx-y) *density*)))
        (set! x xx)
        (set! y yy)))
    (set! max-density (.log js/Math
                            (let [rm (apply max (let [row-max (for [row density] (.apply (.-max js/Math) js/Math row))]
                                                                      row-max))]
                              rm))))

  (reseed [_]
    (.clear _)
    (.seed _)
    (.plot _ 1)
    )

  (soft-light [_ a b]
    (- (+ (bit-shift-right (* a b) 7) (bit-shift-right (* a a) 8)) (bit-shift-right (* a a b) 15)))

  (plot [_ samples]
    (.populate _ samples)
    (let [data (.-data image)]
      (dotimes [i N]
        (dotimes [j N]
          (let [dens (aget density i j)
                idx (* (+ (* i N) j) 4)]
            (aset data (+ idx 3) 255)
            (if (> dens 0)
              (let [light (* (/ (.log js/Math dens) max-density) 255)
                    current (aget data idx)
                    color (.soft-light _ light current)]
                ;(println color)
                (aset data idx color)
                (aset data (+ idx 1) color)
                (aset data (+ idx 2) color))))))
      (.putImageData *context* image 0 0)))
  )


(let [attractor (DeJongAttractor. nil nil 0 0 0 0 0)]
  (.reseed attractor)
  (let [s (Sketch. 0 false attractor nil)]
    (.loopp s)))
